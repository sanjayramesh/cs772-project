from sklearn import datasets
import numpy as np
import math
from sklearn import svm
import pickle

custom_data_home = '/home/sanjay/Desktop/CS772/mldata'

def get_mnist():
    mnist = datasets.fetch_mldata('MNIST original') #, data_home=custom_data_home)
    print mnist.data.shape
    X_train = np.empty(shape=784)
    Y_train = np.array([])
    X_test = np.empty(shape=784)
    Y_test = np.array([])
    x1 = 0
    x2 = 0
    for i, v in enumerate(mnist.target):
        if v==0:
            if x1<200:
                print v, X_test.shape, mnist.data[i].shape
                X_test = np.vstack((X_test, mnist.data[i]))
                Y_test = np.concatenate((Y_test, [-1]))
                x1+=1
            else:
                print v
                X_train = np.vstack((X_train, mnist.data[i]))
                Y_train = np.concatenate((Y_train, [-1]))
        elif v==3:
            print v
            if x2<200:
                print X_test.shape, mnist.data[i].shape
                X_test = np.vstack((X_test, mnist.data[i]))
                Y_test = np.concatenate((Y_test, [1]))
                x2+=1
            else:
                X_train = np.vstack((X_train, mnist.data[i]))
                Y_train = np.concatenate((Y_train, [1]))
        #  print "i", i
        #  if v!=0:
        #      print v
        if v>3:
            break
    return X_train, Y_train, X_test, Y_test

def get_iris():
    iris = datasets.load_iris()
    X_train = iris.data[:40]
    X_train = np.vstack((X_train, iris.data[50:90]))
    #X_train = np.vstack((X_train, iris.data[100:140]))
    X_test = iris.data[40:50]
    #X_test = np.vstack((X_test, iris.data[90:100]))
    X_test = np.vstack((X_test, iris.data[140:]))
    print "X_test shape", X_test.shape

    Y_train = iris.target[:40]
    #Y_train = np.hstack((Y_train, iris.target[50:90]))
    Y_train = np.hstack((Y_train, iris.target[100:140]))
    Y_train = Y_train-1
    print Y_train
    Y_test = iris.target[40:50]
    #Y_test = np.hstack((Y_test, iris.target[90:100]))
    Y_test = np.hstack((Y_test, iris.target[140:150]))
    print "Y_test shape", Y_test.shape
    Y_test = Y_test - 1
    return X_train, Y_train, X_test, Y_test


lam = 2.5

X_train, Y_train, X_test, Y_test = get_mnist()
np.savez('datafile', X_train, Y_train, X_test, Y_test)

# To load the saved train and test datasets
#  npzfile = np.load('datafile.npz')
#  X_train = npzfile['arr_0']
#  Y_train = npzfile['arr_1']
#  X_test = npzfile['arr_2']
#  Y_test = npzfile['arr_3']
#  print X_train.shape, Y_train.shape

X_train = X_train[1:]
X_test = X_test[1:]
N = X_train.shape[0]
D = X_train.shape[1]
print N, D

print "Initializing classifier"
clf = svm.LinearSVC()
print "Fitting SVM"
clf.fit(X_train, Y_train)
s = pickle.dumps(clf)
f = open('svmmodel', 'w')
f.write(s)
f.close()
print "Written model to file"
f = open('svmmodel', 'r')
s = f.read()
f.close()
clf = pickle.loads(s)

# SVM with RBF Kernel
#  print "Initializing classifier with RBF kernel"
#  clf1 = svm.SVC(kernel='rbf')
#  print "Fitting SVM"
#  clf1.fit(X_train, Y_train)
#  clf1.score(X_train, Y_train)
#  y = clf1.predict(X_test)
#
#  print y
#  print Y_test

w = np.random.uniform(low=-30, high=30, size=D)
print "w", w
gamma = np.ones((N, 1))
for _ in range(100):
    print "iteration", _
    for i in range(N):
        gamma[i] = abs(1-Y_train[i]*(w.dot(X_train[i])))
        #  print i, gamma[i]
        #  pass
    sigma = (np.identity(D))*lam
    mu = np.zeros(D)
    for i in range(N):
        #  print i
        sigma = sigma + (np.transpose(X_train[i])*X_train[i])/gamma[i]
        mu = mu + Y_train[i]*(1+1/gamma[i])*(X_train[i])
        #  print i
    sigma = np.linalg.inv(sigma)
    mu = sigma.dot(mu)
    w = mu

print w

acc = 0
for i in range(X_test.shape[0]):
    p1 = math.exp((-2)*max(0, 1+w.dot(X_test[i])))
    p2 = math.exp((-2)*max(0, 1-w.dot(X_test[i])))
    print Y_test[i]+1, p1, p2, w.dot(X_test[i])
print acc

acc = 0
for i in range(X_test.shape[0]):
    p = clf.predict([X_test[i]])
    if p[0]==Y_test[i]:
        acc+=1
    # print p, Y_test[i]

print acc
